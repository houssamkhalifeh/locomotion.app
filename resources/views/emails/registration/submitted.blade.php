@extends('emails.layouts.main')

@section('content')
<p>
    Bonjour,
</p>

<p>
    Merci de votre inscription à LocoMotion!
</p>

<p>
    Votre preuve de résidence va être validée par un-e membre de l'équipe dans les prochains
    jours. Vous serez avisé-e par courriel lorsque que cela sera fait et vous pourrez alors
    démarrer votre participation à LocoMotion!
</p>


<p style="background: #F9CA51;">⚠️Vous allez recevoir un courriel de Noke Pro. Noke Pro, c’est l’application pour débarrer les cadenas. Le courriel reçu sert à activer votre compte Noke Pro. Vous pouvez aussi simplement «&nbsp;inscrire mot de passe oublié&nbsp;» lors de votre première connexion à Noke Pro depuis votre téléphone intelligent.</p>
</p>

<p>À bientôt,</p>

<p>L'équipe LocoMotion<br>
info@locomotion.app</p>
@endsection
