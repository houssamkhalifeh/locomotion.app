@extends('emails.layouts.main')

@section('content')
<p>
    Bonjour!
</p>

<p>
    Vous recevez ce courriel car vous avez récemment débuté votre inscription sur le site
    LocoMotion.app. Ceci est un petit rappel pour finaliser votre profil.
</p>

<p>
    Une fois l'inscription complétée, vous allez recevoir un courriel avec la marche à suivre
    pour participer au programme.
</p>

<p>
    N'hésitez pas à communiquer avec nous pour toute question!
</p>

<p>L'équipe LocoMotion<br>
info@locomotion.app</p>
@endsection
