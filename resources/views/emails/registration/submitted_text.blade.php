@extends('emails.layouts.main_text')

@section('content')
Bonjour,

Merci de votre inscription à LocoMotion!

Votre preuve de résidence va être validée par un-e membre de l'équipe dans les prochains
jours. Vous serez avisé-e par courriel lorsque que cela sera fait et vous pourrez alors
démarrer votre participation à LocoMotion!


Vous allez recevoir un courriel de Noke Pro. Noke Pro, c’est l’application pour débarrer les cadenas. Le courriel reçu sert à activer votre compte Noke Pro. Vous pouvez aussi simplement « inscrire mot de passe oublié » lors de votre première connexion à Noke Pro depuis votre téléphone intelligent.

À bientôt,

L'équipe LocoMotion
info@locomotion.app
@endsection
