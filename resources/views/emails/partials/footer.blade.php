<tr>
    <td style="background-color: #C4C4C4; padding: 10px 0 10px 0;">
        <p style="color: white; font-size: 10px; text-align: center">
            Vous recevez ce courriel parce que vous êtes inscrit sur
            <a href="{{ url('/') }}" target="_blank">LocoMotion</a>.
        </p>
    </td>
</tr>
