<tr>
    <td align="center" style="height: 60px; background-color: white;">
        <div style="text-align: center;">
            <img src="{{ url('/mail-header-logo.png') }}" alt="LocoMotion" style="width: 125px; height: 17px;">
        </div>
    </td>
</tr>
<tr>
    <td align="top" style="background-image: url('{{ url('/mail-header-bg.png') }}'); background-repeat: no-repeat; background-color: #00ADA8; background-position: bottom center; height: 300px; padding: 0 0 100px 0;">
        <h1 style="margin: 0 auto; text-align: center; font-size: 40px; line-height: 48px; color: white;">
            {{ $title }}
        </h1>
    </td>
</tr>
