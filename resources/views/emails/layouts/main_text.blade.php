@include('emails.partials.header_text')

@yield('content')

@include('emails.partials.footer_text')
