export default {
  fr: {
    descriptions: {
      report: "Faites part à votre voisinage de l'état de votre auto. Lorsque vous la partagerez, "
        + 'vous pourrez vous appuyer sur ce document dans vos discussions. Gardez-le bien à jour!',
    },
    engines: {
      fuel: 'essence',
      diesel: 'diesel',
      electric: 'électrique',
      hybrid: 'hybride',
    },
    fields: {
      brand: 'marque',
      engine: 'moteur',
      has_informed_insurer: "l'assureur a été informé",
      insurer: 'assureur',
      is_value_over_fifty_thousand: 'cochez la case si la valeur à neuf de ce véhicule dépasse 50 000$',
      model: 'modèle',
      papers_location: 'emplacement des papiers',
      plate_number: 'numéro de plaque',
      pricing_category: 'catégorie',
      report: "fiche - état de l'auto",
      transmission_mode: 'transmission',
      year_of_circulation: 'année de mise en circulation',
    },
    transmission_modes: {
      automatic: 'automatique',
      manual: 'manuelle',
    },
  },
};

