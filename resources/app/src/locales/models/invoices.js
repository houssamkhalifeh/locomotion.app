import users from './users';

export default {
  fr: {
    'créer une facture': 'créer une facture',
    '{count} facture sélectionnée': 'aucune facture sélectionnée | {count} facture sélectionnée |{count} factures sélectionnées',
    facture: 'facture | factures',
    fields: {
      created_at: 'créée',
      paid_at: 'payée',
      user: users.fr.fields,
    },
  },
};
